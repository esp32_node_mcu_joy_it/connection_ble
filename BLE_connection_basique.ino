#include <BLEDevice.h>  // deux librairies à installer pour utiliser la communication Bluetooth  // installer la librairie "ESP32 BLE Arduino"
#include <BLEServer.h>

/*** Callback pour le serveur  ***/
bool deviceConnected = false;
class MyServerCallbacks : public BLEServerCallbacks{
  void onConnect(BLEServer *MyServer)
  {
    deviceConnected =true;
    Serial.println ("BLE connecté!");
  }
  void onDisconnect(BLEServer *MyServer)
  {
    deviceConnected = false; 
    Serial.println(" BLE deconnecté");
  }
};

void setup() {
  // put your setup code here, to run once:
 Serial.begin(115200);  // liaison série
 Serial.println("Starting BLE");
 
   // creation du BLE DEVICE
  BLEDevice::init("ESP01"); // on inscrit le nom de l'appareil

  //création du serveur
  BLEServer *MyServer =BLEDevice::createServer(); 
  MyServer->setCallbacks(new MyServerCallbacks());

  MyServer->getAdvertising()->start();
  Serial.println("Lancement du serveur");
}

void loop() {
  // put your main code here, to run repeatedly:

}
